package ito.poo.clases;

public class Persona implements Cloneable {
	
	private String nombre;
	private long telefono;
	
	public Persona() {
		super();
	}
	
	public Persona(String nombre, long telefono) {
		super();
		this.nombre = nombre;
		this.telefono = telefono;
	}
	/*********************************************/
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public long getTelefono() {
		return telefono;
	}

	public void setTelefono(long telefono) {
		this.telefono = telefono;
	}
	/*********************************************/
	@Override
	public String toString() {
		return "Persona [nombre=" + nombre + ", telefono=" + telefono + "]";
	}
	
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
}