package ito.poo.app;

import ito.poo.clases.Persona;

public class Myapp {
	
	public static void main(String[] args) {
		
		try {
			Persona persona = new Persona ( "Jose Miguel ", 7267627 );
            System.out.println(persona);
            
            System.out.println();
            
            Persona clonepersona = ( Persona ) persona.clone();
            System.out.println(clonepersona);
            
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
	}	
  }
}