package ito.poo.clases;

import java.time.LocalDate;

public class Ahorro extends Cuenta {

	private float saldo = 0F;
	private LocalDate ultimaActualizacion = null;
	
	public Ahorro(long numCuenta, String cliente, long telefono, float saldo, LocalDate ultimaActualizacion) {
		super(numCuenta, cliente, telefono);	
		this.saldo=saldo;
		this.ultimaActualizacion=ultimaActualizacion;
	}
	public Ahorro(int i, Object object) {
	}
	/*********************************************/		
	public void deposito(float monto) {
		this.saldo=this.saldo+monto;
		this.ultimaActualizacion=LocalDate.now();
	}

	public boolean retiro( float saldo, LocalDate newultimaActualizacion, float monto) {
		boolean retiro=false;
		if(saldo<=this.getSaldo()) {
			retiro=true;
			this.saldo=(this.getSaldo()-monto);
			this.ultimaActualizacion=(newultimaActualizacion);
		}
		else
			System.out.println("Su retiro excede el saldo");
		return retiro;
	}
	/*********************************************/	
	public float getSaldo() {
		return saldo;
	}
	public void setSaldo(float saldo) {
		this.saldo = saldo;
	}
	public LocalDate getUltimaActualizacion() {
		return ultimaActualizacion;
	}
	public void setUltimaActualizacion(LocalDate ultimaActualizacion) {
		this.ultimaActualizacion = ultimaActualizacion;
	}
	/*********************************************/	
	@Override
	public String toString() {
		return "Ahorro [saldo=" + saldo + ", ultimaActualizacion=" + ultimaActualizacion + "]";
	}					
}
