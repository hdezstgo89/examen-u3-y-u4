package ito.poo.clases;

public class Credito extends Cuenta {

	private float limiteCredito = 0F;
	private float creditoUtilizado = 0F;
	/*********************************************/	
	public Credito() {
		super();
	}
	
	public Credito(long numCuenta, String cliente, long telefono, float limiteCredito, float creditoUtilizado) {
		super(numCuenta, cliente, telefono);
		this.limiteCredito = limiteCredito;
		this.creditoUtilizado = creditoUtilizado;
	}
	/*********************************************/	
	public void cargo (float cantidad) {
		if (cantidad <= limiteCredito - creditoUtilizado);
			creditoUtilizado = creditoUtilizado + cantidad; 
	}
				
    public void pago(float cantidad) {
    	if (cantidad <= creditoUtilizado && cantidad>0);
    		creditoUtilizado = creditoUtilizado - cantidad;
	}
	/*********************************************/	
	public float getLimiteCredito() {
		return limiteCredito;
	}
	public void setLimiteCredito(float limiteCredito) {
		this.limiteCredito = limiteCredito;
	}
	public float getCreditoUtilizado() {
		return creditoUtilizado;
	}
	public void setCreditoUtilizado(float creditoUtilizado) {
		this.creditoUtilizado = creditoUtilizado;
	}
	/*********************************************/	
	@Override
	public String toString() {
		return "Credito [limiteCredito=" + limiteCredito + ", creditoUtilizado=" + creditoUtilizado + "]";
	}
}