package ito.poo.clases;

public class Cuenta {

	private long numCuenta = 0L;
	private String cliente = " ";
	private long telefono = 0L;
	/*********************************************/	
	public Cuenta() {
		super();
	}
	public Cuenta(long numCuenta, String cliente, long telefono) {
		super();
			this.numCuenta = numCuenta;
			this.cliente = cliente;
			this.telefono = telefono;
	}
	/*********************************************/	
	public long getNumCuenta() {
		return numCuenta;
	}
	public void setNumCuenta(long numCuenta) {
		this.numCuenta = numCuenta;
	}
	public String getCliente() {
		return cliente;
	}
	public void setCliente(String cliente) {
		this.cliente = cliente;
	}
	public long getTelefono() {
		return telefono;
	}
	public void setTelefono(long telefono) {
		this.telefono = telefono;
	}
	/*********************************************/	
	@Override
	public String toString() {
		return "Cuenta [numCuenta=" + numCuenta + ", cliente=" + cliente + ", telefono=" + telefono + "]";
	}
}