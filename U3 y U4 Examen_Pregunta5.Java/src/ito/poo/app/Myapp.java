package ito.poo.app;

import java.time.LocalDate;
import ito.poo.clases.Ahorro;
import ito.poo.clases.Credito;
import ito.poo.clases.Cuenta;

public class Myapp {
	
	static void run(){
		Cuenta cuenta = new Cuenta(452216,"Yahir Hdez Stgo", 7267627);
		System.out.println(cuenta);
		
		System.out.println();

		Ahorro ahorro= new Ahorro(452216,LocalDate.now());
		System.out.println(ahorro);
		ahorro.deposito(3600);
		System.out.println(ahorro);
		System.out.println(ahorro.retiro(4500f,LocalDate.of(2021,11,12), 0));
		System.out.println(ahorro);		
		System.out.println(ahorro.retiro(0f,LocalDate.of(2021,11,12), 0));
		System.out.println(ahorro);
		
		System.out.println();

		Credito credito=new Credito(1, null, 23, 30, 100);
		System.out.println(credito);
		credito.cargo(10);
		System.out.println(credito);
		credito.pago(22);
		System.out.println(credito);
	}
	public static void main(String[] args) {
		run();
	}
}
